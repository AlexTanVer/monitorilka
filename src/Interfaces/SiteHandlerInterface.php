<?php


namespace App\Interfaces;

use App\Entity\DisablingResources;

/**
 * Интерфейс обработчиков
 *
 * Interface SiteHandlerInterface
 * @package App\Interfaces
 */
interface SiteHandlerInterface
{
    /**
     * @return DisablingResources[]
     */
    public function parseData(): array;

    /**
     * @return string
     */
    public function getDataRoute(): string;

    /**
     * @return string
     */
    public function getSiteAddress(): string;
}
