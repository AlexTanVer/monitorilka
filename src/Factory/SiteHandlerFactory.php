<?php


namespace App\Factory;


use App\Exception\HandlerNotFoundException;
use App\Interfaces\SiteHandlerInterface;

/**
 * Класс фабрики для SiteHandlerInterface
 *
 * Class SiteHandlerFactory
 * @package App\Factory
 */
class SiteHandlerFactory
{
    /** @var SiteHandlerInterface[] */
    private $handlers;

    /**
     * Возвращает конкретную реализацию интерфейса SiteHandlerInterface по его имени
     * Именем является хост сайта
     *
     * @param string $handlerName
     * @return SiteHandlerInterface
     * @throws HandlerNotFoundException
     */
    public function createHandler(string $handlerName): SiteHandlerInterface
    {
        if (isset($this->handlers[$handlerName])) { // Проверяем, есть ли доступный обработчик по переданному имени
            return $this->handlers[$handlerName];
        }

        // Если не смогли найти обработчик, выбрасываем исключение
        throw new HandlerNotFoundException("Handler with name '{$handlerName}' not found");
    }

    /**
     * Добавляет обработчик в массив
     * В дальнейшем используется в методе "createHandler"
     *
     * @param string $alias
     * @param SiteHandlerInterface $handler
     */
    public function addHandler(string $alias, SiteHandlerInterface $handler)
    {
        $this->handlers[$alias] = $handler;
    }
}
