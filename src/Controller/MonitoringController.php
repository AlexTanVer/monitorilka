<?php

namespace App\Controller;

use App\Entity\DisablingResources;
use App\Exception\ReloadException;
use App\SiteHandler\DezkUrHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;

class MonitoringController extends AbstractController
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * MonitoringController constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * При заходе на главную страницу сайта ("/") попадаем в этот метод
     * @Route("/", name="monitoring")
     */
    public function index()
    {
        list($currentDisablingResources, $futureDisablingResources) = $this->entityManager
            ->getRepository(DisablingResources::class)
            ->getSortedDisablingResourcesByResource('Электроснабжение'); // Получаем два массива данных. "Текущие" и "Планируемые"

        return $this->render('monitoring/index.html.twig', [ // Рисуем HTML с передачей параметров
            'currentDisablingResources' => $currentDisablingResources,
            'futureDisablingResources' => $futureDisablingResources,
        ]);
    }

    /**
     * При обновление данных вручную попадаем сюда
     * @Route("/reload", name="reload")
     *
     * @return Response
     * @throws ReloadException
     */
    public function reload()
    {
        $process = new Process(["{$this->getParameter('kernel.project_dir')}/bin/console", 'parse:data', DezkUrHandler::$site]); // Формируем процесс обновления данных
        $process->run(); // запускаем процесс

        if ($process->getExitCode() === 0) { // если процесс завершился с кодом "0" значит успех
            return new Response(200);
        }

        throw new ReloadException("Reload failed. Reason: {$process->getErrorOutput()}"); // Возвращаем ошибку, если получили код завершения отличный от "0"
    }
}
