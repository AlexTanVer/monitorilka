<?php


namespace App\SiteHandler;


use App\Entity\DisablingResources;
use App\Interfaces\SiteHandlerInterface;
use App\SiteHandler\Base\BaseHandler;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class DezkUrHandler
 * @package App\SiteHandler
 */
class DezkUrHandler extends BaseHandler implements SiteHandlerInterface
{
    /** @var string */
    public static $site = 'http://dezk-ur.ru';

    /**
     * Соотношение порядкового номера из строки таблицы HTML со свойством класса
     * @var array
     */
    const MAPPING = [
        0 => 'Address',
        1 => 'Resource',
        2 => 'DateOff',
        3 => 'DateOn',
        4 => 'Reason',
        5 => 'Note'
    ];

    /**
     * Метод для парсинга данных из HTML
     * @return DisablingResources[]
     */
    public function parseData(): array
    {
        $result = []; // массив, содержащий в себе результирующие данные

        $crawler = $this->getCrawler(); // Получаем класс для парсинга HTML
        $crawler->filter('table')->each(function (Crawler $tableCrawler) use (&$result) { // Находим в HTML все теги "table" и запускаем цикл
            $tableCrawler->filter('tr')->each(function (Crawler $trCrawler) use (&$result) { // Находим в HTML все теги "tr" и запускаем цикл

                $rowData = []; // Массив, содержащий в себе данные из одной строки
                $trCrawler->filter('td')->each(function (Crawler $tdCrawler, $i) use (&$rowData) { // Находим в HTML все теги "td" и запускаем цикл
                    $rowData[$i] = $tdCrawler->text(); // Заполняем массив с данными
                });

                if (!empty($rowData)) { // Проверяем, что массив с данными не пуст
                    $result[] = $this->createDisablingResourcesFromData($rowData); // добавляем к результирющему массиву данные по одной строке из таблицы HTML
                }
            });
        });

        return $result;
    }

    /**
     * Возвращает адрес, где необходимо проводить парсинг данных
     * @return string
     */
    public function getDataRoute(): string
    {
        return '/turnoff';
    }

    /**
     * Возвращает хост сайта
     * @return string
     */
    public function getSiteAddress(): string
    {
        return self::$site;
    }

    /**
     * Создаёт и возвращает класс, содержащий в себе данные по одной из строк таблицы HTML
     *
     * @param array $data
     * @return DisablingResources
     * @throws \Exception
     */
    private function createDisablingResourcesFromData(array $data): DisablingResources
    {
        $disablingResources = new DisablingResources(); // Создаём экземпляр класса, для дальнейшего наполнения данными
        $disablingResources->setSource($this->getSiteAddress()); // Заисываем источник данных

        /*
         * Цикл по массиву с данными из одной строки таблицы HTML
         */
        foreach ($data as $key => $datum) {
            if (isset(self::MAPPING[$key])) { // Проверяем, есть ли в соотношении ключ "$key"
                $setMethod = 'set' . self::MAPPING[$key]; // Формируем имя метода, с помощью которого положим данные в класс

                if (in_array($key, [2, 3])) { // У прядковых номеров 2 и 3 тип данных должен быть datetime
                    $datum = new \DateTime($datum);
                }
                $disablingResources->$setMethod($datum); // ЗАписываем данные в класс
            }
        }

        return $disablingResources;
    }
}
