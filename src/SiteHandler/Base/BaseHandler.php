<?php


namespace App\SiteHandler\Base;


use App\Interfaces\SiteHandlerInterface;
use Symfony\Component\DomCrawler\Crawler;

abstract class BaseHandler implements SiteHandlerInterface
{
    public static $site = '';

    /**
     * @return Crawler
     */
    protected function getCrawler(): Crawler
    {
        $content = file_get_contents($this->getSiteAddress() . $this->getDataRoute());
        return $this->createCrawler($content);
    }

    /**
     * @param string $html
     * @return Crawler
     */
    protected function createCrawler(string $html): Crawler
    {
        return new Crawler($html);
    }
}
