<?php

namespace App\Command;

use App\Entity\DisablingResources;
use App\Factory\SiteHandlerFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ParseDataCommand
 * @package App\Command
 */
class ParseDataCommand extends Command
{
    protected static $defaultName = 'parse:data';

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var SiteHandlerFactory */
    private $factory;

    /**
     * ParseDataCommand constructor.
     * @param string|null $name
     * @param EntityManagerInterface $entityManager
     * @param SiteHandlerFactory $factory
     */
    public function __construct(string $name = null, EntityManagerInterface $entityManager, SiteHandlerFactory $factory)
    {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->factory       = $factory;
    }

    protected function configure()
    {
        $this
            ->setDescription('Собирает данные по отключениям ресурсов с указаного сайта')
            ->addArgument('handler', InputArgument::REQUIRED, 'Сайт для сбора данных')
        ;
    }

    /**
     * Команда вызывается через терминал "bin/console parse:data"
     * Это основной метод команды
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \App\Exception\HandlerNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $handler = $this->factory->createHandler($input->getArgument('handler')); // получаем нужный обработчик для парсинга данных
        
        $this->clearData();
        $data = $handler->parseData(); // Парсим данные с сайта

        foreach ($data as $disablingResources) {
            $this->entityManager->persist($disablingResources); // Добавляем строчку к запросу в БД на сохранение
        }

        $this->entityManager->flush(); // Исполняем запрос

        return 0;
    }

    /**
     * Добавляет в запрос к БД, строчки на удаление старых данных
     */
    private function clearData(): void
    {
        $disablingResources = $this->entityManager->getRepository(DisablingResources::class)->findAll(); // Находим все записи в БД
        foreach ($disablingResources as $disablingResource) {
            $this->entityManager->remove($disablingResource); // Добавляем запрос к БД на удаление конкретной записи
        }
    }
}
