<?php


namespace App\Exception;

/**
 * Ошибка, вызываемая, если не смогли определить обработчик для сайта
 *
 * Class HandlerNotFoundException
 * @package App\Exception
 */
class HandlerNotFoundException extends \Exception
{

}
