<?php


namespace App\Exception;

/**
 * Ошибка, вызываемая, если не смогли обновить данные
 *
 * Class ReloadException
 * @package App\Exception
 */
class ReloadException extends \Exception
{

}
