<?php

namespace App\Repository;

use App\Entity\DisablingResources;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Класс, хранящий в себе запросы в БД
 *
 * @method DisablingResources|null find($id, $lockMode = null, $lockVersion = null)
 * @method DisablingResources|null findOneBy(array $criteria, array $orderBy = null)
 * @method DisablingResources[]    findAll()
 * @method DisablingResources[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisablingResourcesRepository extends ServiceEntityRepository
{
    /**
     * DisablingResourcesRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DisablingResources::class);
    }

    /**
     * Возвращает массив, состоящий из двух вложенных массивов
     * Вложенные массивы содержат в себе "Текущие отключения" и "Планируемые отключения"
     *
     * @param string $resource
     * @return array
     * @throws \Exception
     */
    public function getSortedDisablingResourcesByResource(string $resource): array
    {
        $now = new \DateTime();

        /*
         * Запрос в БД на получение "Текущих отключений"
         */
        $currentDisablingResources = $this->createQueryBuilder('dr')
            ->where('dr.dateOff < :nowEnd')
            ->andWhere('dr.dateOff > :nowStart')
            ->andWhere('dr.resource = :resource')
            ->setParameters([
                'resource' => $resource,
                'nowEnd'   => "{$now->format('Y-m-d')} 23:59:59",
                'nowStart' => "{$now->format('Y-m-d')} 00:00:00",
            ])
            ->getQuery()->getResult()
        ;

        /*
         * Запрос в БД на получение "Планируемых отключений"
         */
        $futureDisablingResources = $this->createQueryBuilder('dr')
            ->where('dr.dateOff > :nowEnd')
            ->andWhere('dr.resource = :resource')
            ->setParameters([
                'resource' => $resource,
                'nowEnd'   => "{$now->format('Y-m-d')} 23:59:59",
            ])
            ->getQuery()->getResult()
        ;

        return [$currentDisablingResources, $futureDisablingResources];
    }
}
