<?php


namespace App\DependencyInjection\Compiler;


use App\Factory\SiteHandlerFactory;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class SiteHandlerPass
 * @package App\DependencyInjection\Compiler
 */
class SiteHandlerPass implements CompilerPassInterface
{
    /**
     * Метод исполняется в compile time
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $definition     = $container->findDefinition(SiteHandlerFactory::class);

        /*
         * находим классы, которые помечены тегом "site.handler"
         * Для всех классов, реализующих интерфейс SiteHandlerInterface в Kernel добавляется тег
         */
        $taggedHandlers = $container->findTaggedServiceIds('site.handler');

        /*
         * В цикле каждый найденный класс добавляем в фабрику SiteHandlerFactory с алиасом,
         * который задан статически у каждого класса
         */
        foreach ($taggedHandlers as $id => $taggedHandler) {
            $alias = $id::$site ?? null;
            if (!is_null($alias)) {
                $definition->addMethodCall('addHandler', [$alias, new Reference($id)]);
            }
        }
    }
}
