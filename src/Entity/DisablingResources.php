<?php

namespace App\Entity;

use App\Repository\DisablingResourcesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Класс отражающий таблицу в БД
 * Каждое свойство класса соотносится со столбцом в таблице
 * @ORM\Entity(repositoryClass=DisablingResourcesRepository::class)
 */
class DisablingResources
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resource;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOff;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reason;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $source;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResource(): ?string
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return $this
     */
    public function setResource(string $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateOff(): ?\DateTimeInterface
    {
        return $this->dateOff;
    }

    /**
     * @param \DateTimeInterface $dateOff
     * @return $this
     */
    public function setDateOff(\DateTimeInterface $dateOff): self
    {
        $this->dateOff = $dateOff;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateOn(): ?\DateTimeInterface
    {
        return $this->dateOn;
    }

    /**
     * @param \DateTimeInterface $dateOn
     * @return $this
     */
    public function setDateOn(\DateTimeInterface $dateOn): self
    {
        $this->dateOn = $dateOn;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return $this
     */
    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     * @return $this
     */
    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return $this
     */
    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }
}
